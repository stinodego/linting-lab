import setuptools

setuptools.setup(
    name="linting-lab",
    version="0.1.0",
    author="Stijn de Gooijer",
    description="Linting lab",
    packages=setuptools.find_packages(),
    package_dir={"": "src"},
    install_requires=["pandas"],
    extras_require={
        "dev": [
            "flake8",
            "black",
            "isort",
            "mypy",
        ]
    },
)
